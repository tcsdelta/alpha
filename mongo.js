var winston = require('winston');
var MongoClient = require('mongodb').MongoClient;

var url = "mongodb://ninja:ninja@localhost:27017/shrek";

// this variable is not exported
var dbconn = null;
exports.init = function(init_appserver, callback) {
    MongoClient.connect(url, {
            dbconn: {
                raw: true
            },
            server: {
                poolSize: 5
            }
        },
        function(err, database) {
            if (err != null) {
                callback(err);
            } else {
                dbconn = database;
                init_db_collections();
                init_appserver();
            }
        }
    );

    // initialise database collections for CRUD operations
    function init_db_collections() {
        exports.alpha = dbconn.collection("alpha");
        exports.beta = dbconn.collection("beta");
        exports.gamma = dbconn.collection("gamma");
        exports.delta = dbconn.collection("delta");
    }
};

exports.getCollection = function(name)
{
    return dbconn.collection(name);
}

exports.alpha = null;
exports.beta = null;
exports.gamma = null;
exports.delta = null;
